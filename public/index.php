<?php
error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors','Off');


# FIX CORS SETUP HEADER
$http_origin = $_SERVER['HTTP_ORIGIN'];

if ($http_origin == "https://www.zahnzusatzversicherung-experten.de" || $http_origin == "https://sb.zahnzusatzversicherung-experten.de")
{  
    header("Access-Control-Allow-Origin: $http_origin");
}


use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;

require __DIR__ . '/../vendor/autoload.php';

spl_autoload_register(function ($class) 
{
	if(file_exists(__DIR__ . '/../../sb/zzv_4_4_lts/web/crm/classes/wzm/'.$class.'.class.php')) {
		require_once __DIR__ . '/../../sb/zzv_4_4_lts/web/crm/classes/wzm/'.$class.'.class.php';
	}

	if(file_exists(__DIR__ . '/../../sb/zzv_4_4_lts/web/crm/classes/crm/'.$class.'.class.php')) {
		require_once __DIR__ . '/../../sb/zzv_4_4_lts/web/crm/classes/crm/'.$class.'.class.php';
	}

	if(file_exists(__DIR__ . '/../../sb/zzv_4_4_lts/web/crm/web-modules/project/'.$class.'.class.php')) {
		require_once __DIR__ . '/../../sb/zzv_4_4_lts/web/crm/web-modules/project/'.$class.'.class.php';
	}

	if(file_exists(__DIR__ . '/../../sb/zzv_4_4_lts/web/crm/web-modules/'.$class.'/'.$class.'.class.php')) {
		require_once __DIR__ . '/../../sb/zzv_4_4_lts/web/crm/web-modules/'.$class.'/'.$class.'.class.php';
	}

	if($class == 'databaseResultSet' &&
		file_exists(__DIR__ . '/../../sb/zzv_4_4_lts/web/crm/web-modules/database/'.$class.'.class.php')) {
		require_once __DIR__ . '/../../sb/zzv_4_4_lts/web/crm/web-modules/database/'.$class.'.class.php';
	}
	if(strstr($class, 'Helper') &&
		file_exists(__DIR__ . '/../../sb/zzv_4_4_lts/web/crm/web-modules/helperClasses/'.$class.'.class.php')) {
		require_once __DIR__ . '/../../sb/zzv_4_4_lts/web/crm/web-modules/helperClasses/'.$class.'.class.php';
	}
	if(($class=='L' || $class=='S') &&
		file_exists(__DIR__ . '/../../sb/zzv_4_4_lts/web/crm/web-modules/core/'.$class.'.class.php')) {
		require_once __DIR__ . '/../../sb/zzv_4_4_lts/web/crm/web-modules/core/'.$class.'.class.php';
	}
});


$app = AppFactory::create();

$app->get('/', function (Request $request, Response $response, $args) {
    $response->getBody()->write("Hello world!");
    return $response;
});

$app->get('/getDoc', function (Request $request, Response $response, $args): Response {
	$response->getBody()->write('error');
	return $response;
});


$app->get('/getInsuranceList[/{insurance}]', function (Request $request, Response $response, array $args): Response {
	$c = new contract();
	$tl = $c->getInsuranceList($args['insurance']);

	if(!$tl) 
	{
		header("HTTP/1.0 500 Internal Server Error");
	}

	if(isset($_GET['array']) && $_GET['array'] == 'y')
	{
		print_r($tl); exit;
	}

	$response->getBody()->write(json_encode($tl));
	return $response;
});

$app->get('/getTariffList[/{insurance}]', function (Request $request, Response $response, array $args) {
	$c = new contract();

	if($args['insurance'])
	{
		$tl = $c->getTariffList($args['insurance']);
		$tg = $c->getTariffGuidelines($args['insurance']);

		if($tg)
			$tl[0]['missingTeethInsuranceableLimit'] = $tg[0]['missingTeethInsuranceableLimit'];
		$tl = $tl[0];

	}
	else
	{
		$tl = $c->getTariffList();
	}

	if(!$tl) 
	{
		header("HTTP/1.0 500 Internal Server Error");
	}

	if(isset($_GET['array']) && $_GET['array'] == 'y')
	{
		print_r($tl); exit;
	}


	header('Content-Type: application/json; charset=utf-8');
	print json_encode($tl);

	#$response->getBody()->write(json_encode($tl));
	return $response;
});

/* Alle Tarife */
$app->get('/getTariffListWithRating[/{insurance}]', function (Request $request, Response $response, array $args) {
	$c = new contract();


	if($args['insurance'])
	{
		$tl = $c->getTariffList($args['insurance']);
	} else 
		$tl = $c->getTariffList();



	$wishedBegin = mktime(0, 0, 0, date("m")+1, 1, date("Y"));

	if(!$tl)
		die('no data?!');

	foreach($tl as $id => $val)
	{
		if(isset($val['insurance_id']))
			$in = $c->getTariffInsurance($val['insurance_id']);

		$tr = $c->getTariffRating($val['idt']);
		$tg = $c->getTariffGuidelines($val['idt']);
#print_r($tg);
		$tf = $c->getTariffAVBFile($val['idt']);
		$bbt = $c->getBonusBaseTable($val['idt'], $wishedBegin);

		$t[$val['idt']] = $val;
		$t[$val['idt']]['insurance'] = $in;
		$t[$val['idt']]['rating'] = $tr; 
		$t[$val['idt']]['files'] = $tf;  
		$t[$val['idt']]['bonusBaseTable'] = $bbt;

		if($tg)
		{
			$t[$val['idt']]['medicalCare'] = $tg['medicalCare'];
			$t[$val['idt']]['missingTeethInsuranceableLimit'] = $tg['missingTeethInsuranceableLimit'];
		}

		$class = array('K-KFO' => $val['isChildTariffWithKfoPayment'], 'ZE+ZB+ZR' => $val['showIfDenturesTreatmentProphylaxis'], 'ZE+ZB' => $val['showIfDenturesTreatment'], 'ZE+ZR' => $val['showIfDenturesProphylaxis'], 'ZB+ZR' => $val['showIfTreatmentProphylaxis'], 'ZE' => $val['showIfDentures'], 'ZB' => $val['showIfTreatment'], 'ZR' => $val['showIfProphylaxis'] );
		$t[$val['idt']]['class'] = $class;	
	}



	if(!$t) 
	{
		header("HTTP/1.0 500 Internal Server Error");
	}

	if(isset($_GET['array']) && $_GET['array'] == 'y')
	{
		print_r($t); exit;
	}


	header('Content-Type: application/json; charset=utf-8');
	print json_encode($t);

	#print_r($t);

	#$response->getBody()->write(json_encode($t));
	return $response;
});


$app->get('/getInsuranceYearsTable/{insurance}', function (Request $request, Response $response, array $args) {
	$b = new benefits(0);

	$tl = $b->getInsuranceYearsTableNum($args['insurance']);
	

	if(!$tl) 
	{
		header("HTTP/1.0 500 Internal Server Error");
	}

	if(isset($_GET['array']) && $_GET['array'] == 'y')
	{
		print_r($tl); exit;
	}

	header('Content-Type: application/json; charset=utf-8');
	print json_encode($tl);

	#$response->getBody()->write(json_encode($tl));
	return $response;
});


$app->get('/getTariffGuidelines[/{insurance}]', function (Request $request, Response $response, array $args) {
	$c = new contract();

	if($args['insurance'])
	{
		$tl = $c->getTariffGuidelines($args['insurance']);
	} else 
		$tl = $c->getTariffGuidelines();

	if(!$tl) 
	{
		header("HTTP/1.0 500 Internal Server Error");
	}

	if(isset($_GET['array']) && $_GET['array'] == 'y')
	{
		print_r($tl); exit;
	}

	header('Content-Type: application/json; charset=utf-8');
	print json_encode($tl);

	#$response->getBody()->write(json_encode($tl));
	return $response;
});


$app->get('/getTariffBenefitLimitation[/{insurance}]', function (Request $request, Response $response, array $args) {
	$c = new contract();

	$tl = $c->getTariffBenefitLimitation($args['insurance']);

	if(!$tl) 
	{
		header("HTTP/1.0 500 Internal Server Error");
	}

	if(isset($_GET['array']) && $_GET['array'] == 'y')
	{
		print_r($tl); exit;
	}

	header('Content-Type: application/json; charset=utf-8');
	print json_encode($tl);

	#$response->getBody()->write(json_encode($tl));
	return $response;
});


/* Files */
$app->get('/getTariffFiles/{idt}[/{type}]', function (Request $request, Response $response, array $args) {

	$w = new wzm();


	if(isset($args['type']))
	{
		if($args['type']=="1")
			{ $tl = $w->getTariffAVBFile($args['idt']); }
		if($args['type']=="3")
			{ $tl = $w->getTariffLeistungsbeschreibungFile($args['idt']); }
	} else	
	{
		$tl = $w->getTariffFiles($args['idt']);
	}


	if(isset($_GET['array']) && $_GET['array'] == 'y')
	{
		print_r($tl); exit;
	}
	header('Content-Type: application/json; charset=utf-8');
	print json_encode($tl);

	#$response->getBody()->write(json_encode($tl));
	return $response;
});


/* 4er Vergleiche */
$app->get('/getTariffComparison[/{idt}]', function (Request $request, Response $response, array $args) {

	$m = new modules($args);
	$tl = $m->getComparisonData();

	$tl['bonusTable'] = $m->getBonusBase();
	$tl['files'] = $m->getTariffAVBFile($args['idt']);
	$config = $m->getTariffConfig();

	/* add campaign data if valid */
	if($config[0]['campaignActive'] == 1 || $config[0]['campaignActiveOnLandingpages'] == 1) 
	{
		$tl['config'][0]['campaignActive'] = $config[0]['campaignActive'];
		$tl['config'][0]['campaignActiveOnLandingpages'] = $config[0]['campaignActiveOnLandingpages'];
		$tl['config'][0]['campaignAvailableFrom'] = $config[0]['campaignAvailableFrom'];
		$tl['config'][0]['campaignAvailableTill'] = $config[0]['campaignAvailableTill'];
		$tl['config'][0]['campaignShortText'] = $config[0]['campaignShortText'];
		$tl['config'][0]['campaignLongText'] = $config[0]['campaignLongText'];
		#array_walk_recursive($tl['config'], 'encode_items');
	}

	$tl['config'][0]['offerFromLandingPage'] = $config[0]['offerFromLandingPage'];

	// subrequest not possible on V2 -> rework on V4
	//$tl['performance'] = $app->subRequest('GET', '/getPerformanceData/'.$insurance);

	$series = 0;
	$b = new benefits($series);

	$tlim = $b->getTariffLimitations($args['idt']);

	if(isset($tlim['limglobal']) && is_array($tlim[$tlim['limglobal'][0]]))
	{
		$performance = $tlim[$tlim['limglobal'][0]];
		} elseif ($tlim['dentures']) {
		$performance = $tlim['dentures'];
	}
	$tl['performance'] = $performance;


	if(isset($_GET['array']) && $_GET['array'] == 'y')
	{
		print_r($tl); exit;
	}
	header('Content-Type: application/json; charset=utf-8');
	print json_encode($tl);

	#$response->getBody()->write(json_encode($tl));
	return $response;
});


$app->get('/getTariffRefundExamples[/{idt}]', function (Request $request, Response $response, array $args) {

	$m = new modules($args);
	$tl = $m->getRefundExamples();

	if(isset($_GET['array']) && $_GET['array'] == 'y')
	{
		print_r($tl); exit;
	}
	header('Content-Type: application/json; charset=utf-8');
	print json_encode($tl);

	#$response->getBody()->write(json_encode($tl));
	return $response;
});


$app->get('/getBenefitsData[/{insurance}[/{series}]]', function (Request $request, Response $response, array $args) {

	$arg = array('series' => 0);
	if($args['series'])
		$arg = ['series' => $args['series']];

	$m = new modules($arg);

	$tl = $m->getBenefitsData($args['insurance']);

	if(isset($_GET['array']) && $_GET['array'] == 'y')
	{
		print_r($tl); exit;
	}
	header('Content-Type: application/json; charset=utf-8');
	print json_encode($tl);

	#$response->getBody()->write(json_encode($tl));
	return $response;
});


$app->get('/getRating[/{idt}]', function (Request $request, Response $response, array $args) {

	$m = new modules($args);
	$tl = $m->getRating();

	if(isset($_GET['array']) && $_GET['array'] == 'y')
	{
		print_r($tl); exit;
	}

	header('Content-Type: application/json; charset=utf-8');
	print json_encode($tl);

	#$response->getBody()->write(json_encode($tl));
	return $response;
});


$app->get('/getLongBenefits[/{insurance}[/{series}]]', function (Request $request, Response $response, array $args) {

	if(!$args['series'])
	{
		$series = 0;
	} else $series = $args['series'];

	$b = new benefits($series);

	$tl = $b->getLongBenefits($args['insurance']);

	if(isset($_GET['array']) && $_GET['array'] == 'y')
	{
		print_r($tl); exit;
	}

	header('Content-Type: application/json; charset=utf-8');
	print json_encode($tl);

	#$response->getBody()->write(json_encode($tl));
	return $response;
});


$app->get('/getShortBenefits[/{insurance}[/{series}]]', function (Request $request, Response $response, array $args) {

	if(!$args['series'] || $args['series']=="undefined")
	{
		$series = 0;
	} else $series = $args['series'];

	if($series < 0 || $series > 2) $series = 0;

	$b = new benefits($series);
	$tl = $b->getShortBenefits($args['insurance']);

	if(isset($_GET['array']) && $_GET['array'] == 'y')
	{
		error_reporting(E_ALL);
		print_r($tl, true); exit;
	}

	header('Content-Type: application/json; charset=utf-8');
	print json_encode($tl);

	#$response->getBody()->write(json_encode($tl));
	return $response;
});


$app->get('/getTooltips', function (Request $request, Response $response, array $args) {

	$c = new contract();
	$tl = $c->getTooltips();

	if(isset($_GET['array']) && $_GET['array'] == 'y')
	{
		print_r($tl); exit;
	}

	header('Content-Type: application/json; charset=utf-8');
	print json_encode($tl);

	#$response->getBody()->write(json_encode($tl));
	return $response;
});


$app->get('/getCalculator[/{params:.*}]', function (Request $request, Response $response, array $args)
{
	$args = explode('/', $args['params']);

	if(count($args) > 0)
	{
		switch($args[0])
		{	
			case 0:
			# Erw.
			if(count($args) == 11) 
			{
			if(isset($args[1]) && strlen($args[1]) == 10)
				$birthdate = $args[1];
			if(isset($args[2]) && strlen($args[2]) == 10)
				$wishedBegin = $args[2];
			if(isset($args[3]) && $args[3] >= 0 && $args[3] < 11)
				$missingTeeth = $args[3];
			if(isset($args[4]) && $args[4] >= 0 && $args[4] < 29)
				$replacedTeethRemovable = $args[4];
			if(isset($args[5]) && $args[5] >= 0 && $args[5] < 2)
				$dentalTreatment= $args[5];
			if(isset($args[6]) && $args[6] >= 0 && $args[6] < 29)
				$replacedTeethFix = $args[6];
			if(isset($args[7]) && in_array($args[7], [2,4,5,6,7,8,9]))
				$replacedTeethOlder10Years= $args[7];
			if(isset($args[8]) && $args[8] >= 0 && $args[8] < 2)
				$periodontitis= $args[8];
			if(isset($args[9]) && $args[9] >= 0 && $args[9] < 2)
				$periodontitisHealed= $args[9];
			if(isset($args[10]) && $args[10] >= 0 && $args[10] < 2)
				$biteSplint= $args[10];
			} else 
				die('Anzahl der übergebenen Felder nicht richtig! Bsp.: /getCalculator/0/1995-05-25/1588284000/3/0/0/0/0/0/0/0');

			$args = ['type' => $args[0], 'birthdate' => $birthdate, 
				'wishedBegin' => $wishedBegin,
				'missing-teeth' => $missingTeeth, 
				'replaced-teeth-removable' => $replacedTeethRemovable, 
				'dental-treatment' => $dentalTreatment, 
				'replaced-teeth-fix' => $replacedTeethFix, 
				'replaced-teeth-older-10-years' => $replacedTeethOlder10Years, 
				'periodontitis' => $periodontitis,
				'periodontitis-healed' => $periodontitisHealed,
				'bite-splint' => $biteSplint];

			break;

			case 1:
			# Kinder
			if(count($args) == 5)
			{
			if(isset($args[1]) && strlen($args[1]) == 10)
				$birthdate = $args[1];
			if(isset($args[2]) && strlen($args[2]) == 10)
				$wishedBegin = $args[2];
			if(isset($args[3]) && $args[3] >= 0 && $args[3] < 2)
				$dentalTreatment= $args[3];
			if(isset($args[4]) && $args[4] >= 0 && $args[4] < 2)
				$orthodonticsTreatment= $args[4];

			$args = ['type' => $args[0], 'birthdate' => $birthdate, 
				'wishedBegin' => $wishedBegin,
				'dental-treatment' => $dentalTreatment, 
				'orthodontics-treatment' => $orthodonticsTreatment];

			} else 
				die('Anzahl der übergebenen Felder nicht richtig! Bsp.: /getCalculator/1/1995-05-25/1588284000/0/0');
			break;
		}
	} else die('Anzahl der übergebenen Felder nicht richtig!');



	$m = new modules($args);

	$m->doCalc();

	$tl = $m->getCalculation();
	header('Content-Type: application/json; charset=utf-8');
	#array_walk_recursive($tl, 'encode_items');
	print (json_encode($tl)); exit;
	
	#array_walk_recursive($tl, 'encode_items');
	#$response->getBody()->write(json_encode($tl));
	return $response;
});


/* Add Contract */
$app->post('/addNewContractTest', function(Request $request, Response $response, array $args) {
	$r = $_POST; 
	$c = new contract();

	unset($r['id']);

	if(isset($r['person_gender']) && ($r['person_gender'] == 'Herr' || $r['person_gender'] == 2))
		$r['person_gender'] = 'm';
	if(isset($r['person_gender']) && ($r['person_gender'] == 'Frau' || $r['person_gender'] == 1))
		$r['person_gender'] = 'f';

	if(isset($r['insure_person_gender']) && ($r['insure_person_gender'] == 'Herr' || $r['insure_person_gender'] == 2))
		$r['insure_person_gender'] = 'm';
	if(isset($r['insure_person_gender']) && ($r['insure_person_gender'] == 'Frau' || $r['insure_person_gender'] == 1))
		$r['insure_person_gender'] = 'f';

	if(isset($r['person_other_nationality']) && $r['person_nationality'] == 'other')
	{
		$r['person_nationality'] = $r['person_other_nationality'];
		unset($r['person_other_nationality']);
	} else unset($r['person_other_nationality']);


	if(isset($r['insure_person_other_nationality']) && $r['insure_person_nationality'] == 'other')
	{
		$r['insure_person_nationality'] = $r['insure_person_other_nationality'];
		unset($r['insure_person_other_nationality']);
	} else unset($r['insure_person_other_nationality']);


	if(isset($r['insure_person_firstname']) && isset($r['insure_person_lastname']) && $r['insure_person_firstname'] == '' && $r['insure_person_lastname'] == '')
	{
		foreach($r as $key => $val)
		{
			if(substr($key, 0, 7) == 'insure_')
				unset($r[$key]);
		}
	}

	if(isset($r['person_birthdate']))
	{
		$r['person_birthdate'] = stringHelper::convertDate($r['person_birthdate'], 'sql');
	}
	if(isset($r['insure_person_birthdate']))
	{
		$r['insure_person_birthdate'] = stringHelper::convertDate($r['insure_person_birthdate'], 'sql');
	}

	if(isset($r['wished_begin']))
	{
		$r['wished_begin'] = stringHelper::convertDate($r['wished_begin'], 'sql');
	}

	if(isset($r['display_price']))
	{
		$r['display_price'] = utf8_decode($r['display_price']);
		$r['display_price'] = str_replace(',','.',str_replace('?','',$r['display_price']));
	}


	$allowedFields = array('idt', 'person_gender', 'person_firstname', 'person_lastname', 'person_birthdate', 'person_nationality', 'person_job', 'person_gkv', 
				'insure_person_gender', 'insure_person_firstname', 'insure_person_lastname', 'insure_person_birthdate', 'insure_person_nationality', 'insure_person_job', 'insure_person_gkv',
				'address_street', 'address_postcode', 'address_city', 'contact_phone', 'contact_email', 'contact_msg', 'wished_begin', 'series', 'baseprice', 'surplus', 'display_price', 'browserAgent');

	foreach($r as $k => $v)
	{
		if(in_array($k, $allowedFields))
			$ret[$k] = $v;
	}

// person gender 1 = f
// person gender 2 = m
// geschlecht d - nicht abgedeckt
// gebdat insure_person_birthdate fehlt
// email kommt mit Fehler in Name (utf8!)

	if(isset($r['parameters']))
	{
		if(isset($r['parameters']['series']))
			$r['series'] = $r['parameters']['series'];

		if(isset($r['parameters']['insurancetype']))
			$r['insurancetype'] = $r['parameters']['insurancetype'];

		if(isset($r['parameters']['dentalTreatment']))
			$r['dentalTreatment'] = $r['parameters']['dentalTreatment'];

		if(isset($r['parameters']['orthodonticsTreatment']))
			$r['orthodonticsTreatment'] = $r['parameters']['orthodonticsTreatment'];

		if(isset($r['parameters']['missingTeeth']))
			$r['missingTeeth'] = $r['parameters']['missingTeeth'];

		if(isset($r['parameters']['prosthesis']))
			$r['prosthesis'] = $r['parameters']['prosthesis'];

		if(isset($r['parameters']['implantants']))
			$r['implantants'] = $r['parameters']['implantants'];

		if(isset($r['parameters']['olderTenYrs']))
			$r['olderTenYrs'] = $r['parameters']['olderTenYrs'];

		if(isset($r['parameters']['inCare']))
			$r['inCare'] = $r['parameters']['inCare'];

		if(isset($r['parameters']['parodontitis']))
			$r['parodontitis'] = $r['parameters']['parodontitis'];

		if(isset($r['parameters']['parodontitisCured']))
			$r['parodontitisCured'] = $r['parameters']['parodontitisCured'];

		if(isset($r['parameters']['biteSplint']))
			$r['biteSplint'] = $r['parameters']['biteSplint'];
	}


	if(isset($r['parameters']))
		unset($r['parameters']);
	if(isset($r['browserName']))
		unset($r['browserName']);
	if(isset($r['browserVersion']))
		unset($r['browserVersion']);
	if(isset($r['browserPlatform']))
		unset($r['browserPlatform']);

	$r['created_at'] = date('Y-m-d H:i:s');
	$r['source'] = 'zzv-experten.de';	
	$r['to_transfer'] = 1;
	$r['transferred'] = 0;

	
	$id = $c->addContractToMiddleware($r);

	$response->getBody()->write($id);
	return $response;
});



function encode_items(&$item, $key)
{
    #$item = htmlentities( (string) $item, ENT_QUOTES, 'utf-8', FALSE);
    $item = iconv("Windows-1252", "UTF-8", $item);


    #$item = mb_convert_encoding($item,'Windows-1252','UTF-8');
}


$app->run();